<%@page import="model.User"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype heml>
<html>
<head>

<meta charset="UTF-8">
<title>削除</title>
<link href="css/delete.css" rel="stylesheet" type="text/css" />
</head>
<div class="box2">
	<h5>${userInfo.name}さん
		<a href="LoginServlet" class="roguauto"> ログアウト</a>
	</h5>
</div>

<body>
	<div class="hako">
		<h1>ユーザ削除確認</h1>

		<form action="Delete" method="post">

			<input type="hidden" name="id" value="${user.id}">
			<h5>ログインID : ${user.loginId}</h5>


			<h5>を本当に削除してよろしいでしょうか。</h5>


				<ul class="bt">
               <button>
				<a href="UserList">キャンセル</a></button>

				<button type="submit">OK</button>

			</ul>
		</form>
	</div>
</body>
</html>