<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>

<meta charset="UTF-8">
<title>ユーザ一覧</title>
<link href="./css/user.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="box2">
		<h5>
			${userInfo.name} さん <a href="LoginServlet" class="roguauto">
				ログアウト</a>
		</h5>
	</div>


	<form action="UserList" method="post">
		<div class=hako>
			<p>
			<h1>ユーザ一覧</h1>
			<div class=login>
				<a href="NewUserServlet">新規登録</a>
			</div>


			<div>
				<div class="form-group">
					<label for="id">ログインID</label> <input type="text" name="loginId" id="inputLoginId"
					class="form-group">

				</div>
				<br>

				<div class="form-group">
					<span class="form-group">ユーザー名 </span> <input type="text"
						aria-label="First name" class="form-group" name="name">
				</div>
				<br>

				<div class="form-group">
					<label>生年月日</label> <input type="date" value=""
						class="form-control" name="birthday">
						<label>~</label>
						<input type="date" value=""
						class="form-control" name="birthday2">
				</div>






				<div>
					<button type="submit" class="btn">検索</button>
				</div>
				<table border="1">
					<tr>
						<th>ログインID</th>
						<th>ユーザ名</th>
						<th>生年月日</th>
					</tr>
					<c:forEach var="user" items="${userList}">
						<tr>
							<td>${user.loginId}</td>
							<td>${user.name}</td>
							<td>${user.birthDate}</td>
							<td><a href="Data?id=${user.id}" class="bt1">詳細</a></td>

							<c:if
								test="${userInfo.loginId == user.loginId or userInfo.loginId == 'admin'}">
								<td><a href="UpData?id=${user.id}" class="bt2">更新</a></td>
							</c:if>

							<c:if test="${userInfo.loginId == 'admin'}">
								<td><a href="Delete?id=${user.id}" class="bt3">削除</a></td>
							</c:if>
						</tr>

					</c:forEach>

				</table>
			</div>
		</div>
	</form>
</body>

</html>
