
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!doctype heml>
<html>
<head>

<meta charset="UTF-8">
<title>更新</title>
<link href="css/update.css" rel="stylesheet" type="text/css" />
</head>
<body>

	<div class="box2">
		<h5>${userInfo.name}さん
			<a href="LoginServlet" class="roguauto"> ログアウト</a>
		</h5>
	</div>



	<div class=hako>
		<h1>ユーザ情報更新</h1>

		<form action="UpData" method="post">
			<input type="hidden" name="id" value="${user.id}"
				class="form-control">
			<div class="form-group">
				<label for="exampleInputEmail1">ログインID ${user.loginId}</label>
			</div>

			<div class="form-group">
				<label>パスワード</label> <input type="password" name="password"
					class="form-control">

			</div>


			<div class="form-group">
				<label>パスワード(確認)</label> <input type="password" name="password1"
					class="form-control">
			</div>

			<div class="form-group">
				<label>ユーザ名</label> <input class="form-control" name="name"
					value="${user.name}">
			</div>



			<div class="form-group">
				<label>生年月日</label> <input type="date" class="form-control"
					name="birthDate" value="${user.birthDate}">
			</div>
			<br>


			<div class="btn">
				<button type="submit" class="form-group">更新</button>
			</div>


			<div class=back>
				<a href="UserList">戻る</a>
			</div>
			<c:if test="${errMsg != null}">
				<div class="alert alert-danger" role="alert">${errMsg}</div>
			</c:if>

		</form>
	</div>
</body>


</html>