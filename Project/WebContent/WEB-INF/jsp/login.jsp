<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
<div class="hako">

	<body>
		<div class="box">
			<h1>ログイン画面</h1>
		</div>
		<form action="LoginServlet" method="post">
			<div class="form-group">

				<input type="text" name="loginId" id="inputLoginId"
					class="form-group" placeholder="ログインID" requiredautofocus>

				<input type="password" name="password" id="inputPassword"
					class="form-group" placeholder="パスワード" required>

				<div>
					<c:if test="${errMsg != null}">
						<div class="alert alert-danger" role="alert">${errMsg}</div>
					</c:if>
					<button type="submit" class="btn btn-primary">ログイン</button>
				</div>
			</div>



		</form>

	</body>
</div>
</html>


