<%@page import="model.User"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<!DOCTYPE html>
<html>
<head>

<meta charset="UTF-8">
<title>ユーザ情報詳細参照</title>
<link href="css/data.css" rel="stylesheet" type="text/css" />
</head>
<body>

	<div class="box2">
		<h5>${userInfo.name}
			さん <a href="LoginServlet" class="roguauto"> ログアウト</a>
		</h5>
	</div>

	<form>

		<div class=hako>
			<p>
			<h1>ユーザ情報詳細参照</h1>

			<div class="form-group row">
				<label for="exampleInputEmail1">ログインID ${user.loginId}</label>


			</div>

			<div class="form-group">
				<label for="exampleInputPassword1">ユーザー名 ${user.name}</label>


			</div>

			<div class="form-group">
				<label for="exampleInputPassword1">生年月日 ${user.birthDate}</label>

			</div>


			<div class="form-group">
				<label class="input-group-text">登録日時 ${user.createDate}</label>

			</div>


			<div class="form-group">
				<label>更新日時 ${user.updateDate}</label>

			</div>
			<br>


			<div class=back>
				<a href="UserList">戻る</a>
			</div>
		</div>
	</form>
</body>

</html>
