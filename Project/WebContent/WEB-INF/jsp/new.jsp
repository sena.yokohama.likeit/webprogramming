<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>

	<meta charset="UTF-8">
	<title>新規登録</title>
    <link href="css/new.css" rel="stylesheet" type="text/css" />
</head>
<body>

     <div class="box2">
      <h5>${userInfo.name} さん
         <a  href="LoginServlet"class="roguauto">
             ログアウト</a></h5>
     </div>
 <div class=hako>

     <p>
    <h1>ユーザ新規登録</h1>

 <form action="NewUserServlet" method="post">
       <div class="form-group row">
          <label for="exampleInputEmail1" >ログインID</label>
      <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="loginId" >

           </div>

  <div class="form-group">
    <label for="exampleInputPassword1">パスワード</label>

 <input type="password" class="form-control" id="exampleInputPassword1" name="password">
   </div>

       <div class="form-group">
    <label for="exampleInputPassword1">パスワード(確認)</label>

 <input type="password" class="form-control" id="exampleInputPassword1" name="password1" >
   </div>


      <div class="form-group">
    <label class="input-group-text">ユーザー名  </label>

   <input type="text" aria-label="First name" class="form-control" name="name">
  </div>


<div class="form-group">
  <label>生年月日</label>

  <input type="date" class= "form-control" name="birthDate">
     </div>
    <br>

     <div class="btn">
  <button type="submit" >登録
      </button>
</div>


</form>
     <div class= back>
     <a href="UserList">戻る</a>
     </div>

     	<c:if test="${errMsg != null}">
						<div class="alert alert-danger" role="alert">${errMsg}</div>
					</c:if>
    </div>
         </body>

</html>

